ARG CI_REGISTRY
FROM ${CI_REGISTRY}/sw4j-net/jdk17:latest

ARG URL=https://repo.scala-sbt.org/scalasbt/debian
ARG KEY=2EE0EA64E40A89B84B2DF73499E82A75642AC823

RUN <<EOF
apt-get update
apt-get -y upgrade
apt-get -y install apt-transport-https gnupg2 curl
echo "deb ${URL} all main" | tee /etc/apt/sources.list.d/sbt.list
echo "deb ${URL} /" | tee /etc/apt/sources.list.d/sbt_old.list
curl -sL "https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x${KEY}" | apt-key add
apt-get update
apt-get -y install sbt
sbt -Dsbt.rootdir=true <<EOFSBT
exit
EOFSBT
EOF
